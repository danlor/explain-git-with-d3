﻿expliquer-git-avec-d3
=====================
Utilisez D3 pour visualiser les opérations simples de branchement git.

Ce projet simple est conçu pour aider les gens à comprendre visuellement certains concepts fondamentaux de git.

C'est ma première tentative d'utilisation de SVG et de D3. J'espère que cela vous sera utile.

La page peut être consultée via: http://onlywei.github.io/explain-git-with-d3/
